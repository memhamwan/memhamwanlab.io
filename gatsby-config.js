module.exports = {
  pathPrefix: `/`,
  siteMetadata: {
    title: `HamWAN Memphis Metro`,
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
    links: [
      {
        name: `Wiki`,
        href: `https://gitlab.com/groups/memhamwan/-/wikis/home`,
      },
      {
        name: `Dashboard`,
        href: `https://dashy.memhamwan.net`,
      },
      {
        name: `Chat`,
        href: `https://discord.gg/9PXZCrqzJX`,
      },
      {
        name: `Donate`,
        href: `https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=8HFSU6MSFLUF8&source=url`,
      },
    ],
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `rgb(100,149,255)`,
        theme_color: `rgb(100,149,255)`,
        display: `minimal-ui`,
        icon: `src/images/favicon.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-theme-ui`,
    `gatsby-plugin-image`,
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
