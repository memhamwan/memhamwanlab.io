export type Links = Array<{
  href: string
  name: string
}>
