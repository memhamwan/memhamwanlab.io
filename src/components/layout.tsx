import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Header from "./header"
import "./layout.css"

type LayoutProps = {
  children?: React.ReactNode
  image?: React.ReactNode
}

const Layout: React.FC<LayoutProps> = ({ children, image }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
          links {
            name
            href
          }
        }
      }
    }
  `)

  return (
    <>
      <Header siteTitle={data.site.siteMetadata.title} image={image} links={data.site.siteMetadata.links} />
      <div
        style={{
          margin: `0 auto`,
          maxWidth: 960,
          padding: `0px 1.0875rem 1.45rem`,
          paddingTop: 0,
        }}
      >
        <main>{children}</main>
        <footer>
          © {new Date().getFullYear()} {data.site.siteMetadata.title}.{` `}
          <a href="https://gitlab.com/memhamwan/memhamwan.gitlab.io/-/tree/master">Edit on Gitlab</a>.
        </footer>
      </div>
    </>
  )
}

export default Layout
